# Biblioteca EnlaceLibre - Generado con GeLiBA


Este proyecto es el sitio web generado con el Script Generador de Bibliotecas Libres Automatizadas Ge.Li.B.A.

Se encuentra alojado en una Raspberry Pi Zero en mi casa y permite descargar libros con licencias libres.
La idea es ir reuniendo documentos que vaya encontrando y sumar al sitio, para ofrecer para su descarga.

Para más info de este proyecto, podés ingresar aquí:
[GeLiBA](https://gitlab.com/enlacepilar/generador-libre-de-bibliotecas-automatizadas)

La web de GeLiBA (también alojada en la Raspberry):

[Sitio web GeLiBA](https://geliba.enlacepilar.com.ar)
